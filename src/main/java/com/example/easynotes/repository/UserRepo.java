package com.example.easynotes.repository;

import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.easynotes.model.Tasks;
import com.example.easynotes.model.Users;

public interface UserRepo extends JpaRepository<Users,Long> {
	
	@Query(value="Select userid from users u WHERE u.first_name = :firstname AND u.last_name = :lastname ",nativeQuery=true)
	Long getUserIdByName(@Param("firstname") String firstname,@Param("lastname") String lastname);

}
