package com.example.easynotes.repository;

import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.example.easynotes.model.Projects;

public interface ProjectsRepo extends JpaRepository<Projects, Long>{

	@Query(value="Select * from projects",nativeQuery=true)
	ArrayList<Projects> getAllProjects();
	
	

}
