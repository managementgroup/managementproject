package com.example.easynotes.repository;

import java.awt.List;
import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.easynotes.model.Teams;

public interface TeamsRepo extends JpaRepository<Teams,Long> {
	
	@Query(value="Select * from teams",nativeQuery=true)
	ArrayList<Teams> getTeamsList();

}
