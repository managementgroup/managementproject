package com.example.easynotes.repository;

import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.easynotes.model.Requirements;
import com.example.easynotes.model.Risks;

public interface RisksRepo extends JpaRepository<Risks,Long> {

	@Query(value="Select * from risks",nativeQuery=true)
	ArrayList<Risks> getAllRisks();

	@Query(value="Select * from risks u WHERE u.FK_ProjectId = :ProjectId",nativeQuery=true)
	ArrayList<Risks> getProjectRisks(@Param("ProjectId") Long ProjectId);

}
