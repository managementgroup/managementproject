package com.example.easynotes.repository;

import java.awt.List;
import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.easynotes.model.Tasks;

public interface TasksRepo extends JpaRepository<Tasks, Long> {
	
	
	@Query(value="Select * from task_hours t WHERE t.user_id = :userId",nativeQuery=true)
	ArrayList<Tasks> getTasksByUserId(@Param("userId") Long userId);
	
	

}
