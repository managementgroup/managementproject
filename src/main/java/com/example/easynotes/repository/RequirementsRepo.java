package com.example.easynotes.repository;

import java.util.ArrayList;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.easynotes.model.Requirements;
import com.example.easynotes.model.Tasks;


public interface RequirementsRepo extends JpaRepository<Requirements,Long> {
	
	@Query(value="Select * from requirements" ,nativeQuery=true)
	ArrayList<Requirements> getAllRequirements();
	
//	@Query(value="INSERT INTO `requirements ` (`RequirementId`,`Requirement`,`requirement_type`) VALUES (:requirementId,:Requirement,:requirement_type)")
//	public void createRequirement();
	
	@Modifying
	@Query(value="Update requirements r SET r.Requirement = :requirement WHERE r.requirement_id=:requirementid",nativeQuery=true)
	void updateRequirement(@Param(value="requirement") String requirement,@Param(value="requirementid") Long requirementid);
}
