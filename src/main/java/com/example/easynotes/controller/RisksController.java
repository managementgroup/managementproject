package com.example.easynotes.controller;

import com.example.easynotes.model.Projects;
import com.example.easynotes.model.Requirements;
import com.example.easynotes.model.Risks;
import com.example.easynotes.repository.RisksRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/risks")

public class RisksController {
	
	@Autowired
	 RisksRepo riskRepo;
	
	@GetMapping("/all")
	public List<Risks> getAllRisks()
	{
		ArrayList<Risks> riskList = riskRepo.getAllRisks();
		
		
		
		return riskList;
		
		
	}
	
	@GetMapping("/{projectId}/all")
	public List<Risks> getAllProjectRisks(@PathVariable(value="projectId") Long projectId)
	{
		ArrayList<Risks> projectRisks = riskRepo.getProjectRisks(projectId);
		
		//sks risk = riskRepo.findOne(projectId);
		
		return projectRisks;
		
	}
	
@PostMapping("/create")
	
	public Risks createRequirement(@Valid @RequestBody Risks risk) {
		
		return riskRepo.save(risk);
		
	}

@GetMapping("/{id}")
public ResponseEntity<Risks> getRiskById(@PathVariable(value="id") Long riskId)
{
	Risks risk = riskRepo.findOne(riskId);
	
	if(risk == null)
	{
		return ResponseEntity.notFound().build();
	}
	
	return ResponseEntity.ok().body(risk);
}

@PutMapping("update/{id}")
public Risks updateRisk(@PathVariable(value="id") Long riskId, @Valid @RequestBody Risks riskDetails){
	
	
	Risks risk = riskRepo.findOne(riskId);
	
	risk.setRisk(riskDetails.getRisk());
	risk.setRiskStatus(riskDetails.getRiskStatus());
	risk.setProjectId(riskDetails.getProjectId());
	
	Risks updatedRisk = riskRepo.save(risk);
	return updatedRisk;
}

@PutMapping("delete/{id}")
public ResponseEntity<Risks> deleteRisk(@PathVariable(value="id")Long riskId)
{
	Risks risk =riskRepo.findOne(riskId);
	
	riskRepo.delete(risk);
	
	return ResponseEntity.ok().build();
}

}
