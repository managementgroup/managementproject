package com.example.easynotes.controller;

import com.example.easynotes.model.Requirements;
import com.example.easynotes.model.Users;
import com.example.easynotes.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/users")

public class UserController {
	
	@Autowired
	 UserRepo userrepo;
	
	@GetMapping("/all")
	public List<Users> getAllUsers()
	{
		List<Users> userList = userrepo.findAll();
		
		return userList;
		
		
	}
	
@PostMapping("/create")
	
	public Users createUser(@Valid @RequestBody Users user) {
		
		return userrepo.save(user);
		
	}

@GetMapping("/{id}")
public ResponseEntity<Users> getUserById(@PathVariable(value="id") Long userId)
{
	Users user = userrepo.findOne(userId);
	
	if(user == null)
	{
		return ResponseEntity.notFound().build();
	}
	
	return ResponseEntity.ok().body(user);
}



@PutMapping("update/{id}")
public Users updateUser(@PathVariable(value="id") Long userId, @Valid @RequestBody Users userDetails){
	
	
	Users user = userrepo.findOne(userId);
	
	user.setFirstName(userDetails.getFirstName());
	user.setLastname(userDetails.getLastname());
	
	Users updatedUser = userrepo.save(user);
	return updatedUser;
}

@GetMapping("/getUserId/{firstname}/{lastname}")
public Long getUserIdByName(@PathVariable(value="firstname") String firstname, @PathVariable(value="lastname") String lastname)
{
	Long userid = userrepo.getUserIdByName(firstname, lastname);
	
	return userid;
}

@PutMapping("delete/{id}")
public ResponseEntity<Users> deleteUser(@PathVariable(value="id") Long userId)
{
	Users user = userrepo.findOne(userId);
	
	userrepo.delete(userId);
	
	return ResponseEntity.ok().build();
}

}
