package com.example.easynotes.controller;

import com.example.easynotes.model.Projects;
import com.example.easynotes.model.Requirements;
import com.example.easynotes.repository.ProjectsRepo;
import com.example.easynotes.repository.RequirementsRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/projects")

public class ProjectsController {
	
	@Autowired
	 ProjectsRepo projectsrepo;
	
	@GetMapping("/all")
	public List<Projects> getAllProjects()
	{
		ArrayList<Projects> projectsList = projectsrepo.getAllProjects();
		
		return projectsList;
		
		
	}
	
@PostMapping("/create")
	
	public Projects createProject(@Valid @RequestBody Projects project) {
		
		return projectsrepo.save(project);
		
	}

@GetMapping("/{id}")
public ResponseEntity<Projects> getProjectById(@PathVariable(value="id") Long projectId)
{
	Projects project = projectsrepo.findOne(projectId);
	
	if(project == null)
	{
		return ResponseEntity.notFound().build();
	}
	
	return ResponseEntity.ok().body(project);
}

@PutMapping("update/name/{id}")
public Projects updateProjectName(@PathVariable(value="id") Long projectId, @Valid @RequestBody Projects projectDetails){
	
	
	Projects project = projectsrepo.findOne(projectId);
	
	project.setProjectName(projectDetails.getProjectName());
	
	Projects updatedProjectName = projectsrepo.save(project);
	return updatedProjectName;
}

@PutMapping("update/description/{id}")
public Projects updateProjectDescription(@PathVariable(value="id") Long projectId, @Valid @RequestBody Projects projectDetails){
	
	
	Projects project = projectsrepo.findOne(projectId);
	
	project.setDescription(projectDetails.getDescription());
	
	Projects updatedProjectDescription = projectsrepo.save(project);
	return updatedProjectDescription;
}

@PutMapping("update/owner/{id}")
public Projects updateProjectOwner(@PathVariable(value="id") Long projectId, @Valid @RequestBody Projects projectDetails){
	
	
	Projects project = projectsrepo.findOne(projectId);
	
	project.setProjectOwner(projectDetails.getProjectOwner());
	
	Projects updatedProjectOwner = projectsrepo.save(project);
	return updatedProjectOwner;
}

@PutMapping("update/all/{id}")
public Projects updateAll(@PathVariable(value="id") Long projectId, @Valid @RequestBody Projects projectDetails){
	
	
	Projects project = projectsrepo.findOne(projectId);
	
	project.setProjectOwner(projectDetails.getProjectOwner());
	project.setDescription(projectDetails.getDescription());
	project.setProjectName(projectDetails.getProjectName());
	
	Projects updatedProjectOwner = projectsrepo.save(project);
	return updatedProjectOwner;
}

@DeleteMapping("delete/{id}")
public ResponseEntity<Projects> deleteProject(@PathVariable(value="id") Long projectId, @Valid @RequestBody Projects projectDetails){
	
	
	Projects project = projectsrepo.findOne(projectId);

	projectsrepo.delete(project);
	return ResponseEntity.ok().build();
}



}
