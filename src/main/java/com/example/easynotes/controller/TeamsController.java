package com.example.easynotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.easynotes.model.Requirements;
import com.example.easynotes.model.Tasks;
import com.example.easynotes.model.Teams;
import com.example.easynotes.repository.TeamsRepo;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/teams")

public class TeamsController {
	
	@Autowired
	 TeamsRepo teamsrepo;
	
	@GetMapping("/all")
	public ArrayList<Teams> getAllTasks()
	{
		return teamsrepo.getTeamsList();
		
		
	}
	
	@GetMapping("/{teamid}")
	public ResponseEntity<Teams>getTeamDetails(@PathVariable(value="teamid") Long teamId)
	{
		 Teams team = teamsrepo.findOne(teamId);
		 
		
		 if(team == null)
			{
				return ResponseEntity.notFound().build();
			}
			
			return ResponseEntity.ok().body(team);
	}
	

	
	

	@PostMapping("update/{teamid}")
	public Teams updateTeam(@PathVariable(value="teamid") Long teamId, @Valid @RequestBody Teams teamDetails){
		
		 
		Teams team = teamsrepo.findOne(teamId);
		
		
		
		
		
		team.setTeamName(teamDetails.getTeamName());
		team.setProjectId(teamDetails.getProjectId());
		
		
		
		
		Teams updatedTeam= teamsrepo.save(team);
		
		return updatedTeam;
		
		
	}
	
	@PostMapping("/create")
	
	public Teams createTeam(@Valid @RequestBody Teams team) {
		
		return teamsrepo.save(team);
		
	}
	
	@PostMapping("/delete/{id}")
	
	public ResponseEntity<Teams> deleteTeam(@PathVariable(value="id") Long teamId)
	{
		Teams team = teamsrepo.findOne(teamId);
		
		teamsrepo.delete(team);
		
		return ResponseEntity.ok().build();
	}
	
	
	

}
