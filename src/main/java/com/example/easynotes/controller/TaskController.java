package com.example.easynotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.easynotes.model.Risks;
import com.example.easynotes.model.Tasks;
import com.example.easynotes.model.Users;
import com.example.easynotes.repository.TasksRepo;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/tasks")

public class TaskController {
	
	@Autowired
	 TasksRepo taskrepo;
	
	@GetMapping("/all")
	public List<Tasks> getAllTasks()
	{
		return taskrepo.findAll();
		
		
	}
	
	@GetMapping("/{id}/allTasks")
	public List<Tasks> getTasksforUser(@PathVariable(value="id") Long userId)
	{
		ArrayList<Tasks> allUserTasks = taskrepo.getTasksByUserId(userId);
		
		return allUserTasks;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Tasks> getTaskByUserId(@PathVariable(value="id") Long userId)
	{
		Tasks task = taskrepo.findOne(userId);
		
		if(task == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(task);
	}
	
	@GetMapping("/status/{id}")
	public String getStatusByTaskId(@PathVariable(value="id") Long taskId)
	{
		Tasks taskdetails = taskrepo.findOne(taskId);
		
		String status = taskdetails.getStatus();
		
		return status;
	}
	

	@PutMapping("update/{userid}/{taskid}")
	public ResponseEntity<Tasks> updateUserTask(@PathVariable(value="taskid") Long TaskId,@PathVariable(value="userid") Long userId, @Valid @RequestBody Tasks taskDetails){
		
		 
		taskDetails = taskrepo.findOne(TaskId);
		taskDetails = taskrepo.findOne(userId);
		
		if(taskDetails == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		
		taskDetails.setTaskName(taskDetails.getTaskName());
		
		
		
		Tasks updatedTask= taskrepo.save(taskDetails);
		
		return ResponseEntity.ok(updatedTask);
		
		
	}
	
//	@PutMapping("update/{id}")
//	public Tasks updateTasks(@PathVariable(value="id") Long userId, @PathVariable(value="taskId") Long taskId , @Valid @RequestBody Users userDetails){
//		
//		
////		Tasks task = taskrepo.fi;
////		task 
////		
////		user.setFirstName(userDetails.getFirstName());
////		user.setLastname(userDetails.getLastname());
////		
////		Users updatedUser = userrepo.save(user);
////		return updatedUser;
//	}
	
	@PostMapping("/update/{id}/all")
	public Tasks updateAllTasks(@PathVariable(value="id") Long taskId,@Valid @RequestBody Tasks taskDetails)
	{
		
		Tasks task = taskrepo.findOne(taskId);
		
		task.setHours(taskDetails.getHours());
		task.setStatus(taskDetails.getStatus());
		task.setTaskName(taskDetails.getTaskName());
		task.setTimeType(taskDetails.getTimeType());
		task.setUserId(taskDetails.getUserId());
		
		
		
		Tasks updatedTask = taskrepo.save(task);
		
		
		return updatedTask;
	}
	
	@PostMapping("/create")
	public Tasks createNewTaskforUser(@Valid @RequestBody Tasks task)
	{
		return taskrepo.save(task);
		
		
	}
	

	@PutMapping("updateHours/{userid}/{taskid}")
	public Tasks updateUserHours(@PathVariable(value="taskid") Long TaskId,@PathVariable(value="userid") Long userId, @Valid @RequestBody Tasks taskDetails){
		
		 
		Tasks task = taskrepo.findOne(TaskId);
		task = taskrepo.findOne(userId);
		
		
		
		
		taskDetails.setHours(taskDetails.getHours());
		
		
		Tasks updatedTaskHour= taskrepo.save(taskDetails);
		
		return updatedTaskHour;
		
		
	}
	
	@PutMapping("delete/task/{id}")
	public ResponseEntity<Tasks> deleteTaskByTaskId(@PathVariable(value="id") Long taskId)
	{
		Tasks task= taskrepo.findOne(taskId);
		
		taskrepo.delete(task);
		
		return ResponseEntity.ok().build();
	}
	
	@PutMapping("delete/user/{id}")
	public ResponseEntity<Tasks> deleteTaskByUserID(@PathVariable(value="id") Long userId)
	{
		Tasks task = taskrepo.findOne(userId);
		
		taskrepo.delete(task);
		
		return ResponseEntity.ok().build();
	}

}
