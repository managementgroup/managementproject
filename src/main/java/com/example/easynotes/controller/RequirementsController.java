package com.example.easynotes.controller;


import com.example.easynotes.model.Requirements;
import com.example.easynotes.repository.RequirementsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/requirements")

public class RequirementsController {
	
	@Autowired
	 RequirementsRepo requirementsrepo;
	
	@GetMapping("/all")
	public List<Requirements> getAllRequirements()
	{
		ArrayList<Requirements> requirementsList = requirementsrepo.getAllRequirements();
		
		return requirementsList;
		
		
	}
	
@PostMapping("/create")
	
	public Requirements createRequirement(@Valid @RequestBody Requirements requirement) {
		
		return requirementsrepo.save(requirement);
		
	}

@GetMapping("/{id}")
public ResponseEntity<Requirements> getRequirementById(@PathVariable(value="id") Long requirementId)
{
	Requirements requirement = requirementsrepo.findOne(requirementId);
	
	if(requirement == null)
	{
		return ResponseEntity.notFound().build();
	}
	
	return ResponseEntity.ok().body(requirement);
}

@PutMapping("update/{id}")
public Requirements updateRequirement(@PathVariable(value="id") Long requirementId, @Valid @RequestBody Requirements requirementDetails){
	
	
	Requirements req = requirementsrepo.findOne(requirementId);
	
	req.setRequirement(requirementDetails.getRequirement());
	req.setProjectId(requirementDetails.getProjectId());
	req.setRequirement_type(requirementDetails.getRequirement_type());;
	
	Requirements updatedRequirement = requirementsrepo.save(req);
	return updatedRequirement;
}

@PutMapping("delete/{id}")
public ResponseEntity<Requirements> deleteRequirement(@PathVariable(value="id") Long requirementId)
{
	Requirements req = requirementsrepo.findOne(requirementId);
	
	requirementsrepo.delete(req);
	
	return ResponseEntity.ok().build();
}

}
