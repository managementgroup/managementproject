package com.example.easynotes.model;

public class TaskIdentity {
	
	
	private Long taskid;
	
	private Long userId;
	
	public TaskIdentity() {}
    public TaskIdentity (Long taskid, Long userId)
    {
    	this.setTaskid(taskid);
    	this.setUserId(userId);
    }
	public Long getTaskid() {
		return taskid;
	}
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
