package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="projects")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)

public class Projects {
	
	Projects(){}
	
	Projects(Long projectId, String projectName, Long projectOwner, String description)
	{
		this.ProjectId=projectId;
		this.ProjectName=projectName;
		this.ProjectOwner=projectOwner;
		this.Description=description;
	}
	
	@Id
	@Column(name="ProjectId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ProjectId;
	
	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return ProjectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.ProjectId = projectId;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return ProjectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.ProjectName = projectName;
	}

	/**
	 * @return the projectOwner
	 */
	public Long getProjectOwner() {
		return ProjectOwner;
	}

	/**
	 * @param projectOwner the projectOwner to set
	 */
	public void setProjectOwner(Long projectOwner) {
		this.ProjectOwner = projectOwner;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.Description = description;
	}

	@Column(name="ProjectName")
	private String ProjectName;
	
	@Column(name="ProjectOwner")
	private Long ProjectOwner;
	
	@Column(name="Description")
	private String Description;

}
