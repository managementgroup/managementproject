package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="task_hours")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)

public class Tasks {
	
	
	  	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name="taskid")
	    private Long taskid;

		
	  	
	  	public Tasks() {}

	    public Tasks(Long taskid,String taskName,String hours, String timeType, Long userId,String status)
	    {
	            this.taskid=taskid;
	            this.taskName=taskName;
	            this.hours=hours;
	            this.timeType=timeType;
	            this.userId = userId;
	            this.status=status;
	    }
	    
	    @Column(name="status")
	    private String status;
	    
	    /**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}

		/**
		 * @param status the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}

		@Column(name = "task_name")
	    private String taskName;

	    /**
		 * @return the taskid
		 */
		public Long getTaskid() {
			return taskid;
		}

		/**
		 * @param taskid the taskid to set
		 */
		public void setTaskid(Long taskid) {
			this.taskid = taskid;
		}

		/**
		 * @return the taskName
		 */
		public String getTaskName() {
			return taskName;
		}

		/**
		 * @param taskName the taskName to set
		 */
		public void setTaskName(String taskName) {
			this.taskName = taskName;
		}

		/**
		 * @return the hours
		 */
		public String getHours() {
			return hours;
		}

		/**
		 * @param hours the hours to set
		 */
		public void setHours(String hours) {
			this.hours = hours;
		}

		/**
		 * @return the timeType
		 */
		public String getTimeType() {
			return timeType;
		}

		/**
		 * @param timeType the timeType to set
		 */
		public void setTimeType(String timeType) {
			this.timeType = timeType;
		}

		/**
		 * @return the userId
		 */
		public Long getUserId() {
			return userId;
		}

		/**
		 * @param userId the userId to set
		 */
		public void setUserId(Long userId) {
			this.userId = userId;
		}

		@Column(name = "hours")
	    private String hours;

	    @Column(name = "time_type")
	    private String timeType;

	    @Column(name = "user_id")
	    private Long userId;

}
