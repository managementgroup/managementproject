package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="requirements")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)

public class Requirements {
	
	public Requirements() {}
	
	public Requirements(Long requirementId, String requirement,String requirement_type, Long projectid)
	{
		this.RequirementId=requirementId;
		this.requirement=requirement;
		this.requirement_type=requirement_type;
		this.ProjectId=projectid;
	}

	/**
	 * @return the requirement_id
	 */
	

	/**
	 * @return the requirement
	 */
	public String getRequirement() {
		return requirement;
	}

	/**
	 * @param requirement the requirement to set
	 */
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	/**
	 * @return the requirement_type
	 */
	public String getRequirement_type() {
		return requirement_type;
	}

	/**
	 * @param requirement_type the requirement_type to set
	 */
	public void setRequirement_type(String requirement_type) {
		this.requirement_type = requirement_type;
	}

	@Id
	@Column(name="requirement_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long RequirementId;
	
	/**
	 * @return the requirementId
	 */
	public Long getRequirementId() {
		return RequirementId;
	}

	/**
	 * @param requirementId the requirementId to set
	 */
	public void setRequirementId(Long requirementId) {
		RequirementId = requirementId;
	}

	@Column(name="Requirement")
	private String requirement;
	
	@Column(name="requirement_type")
	private String requirement_type;
	
	@Column(name="ProjectId")
	private Long ProjectId;

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return ProjectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.ProjectId = projectId;
	}
	

	
}
