package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="users")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)


public class Users {
	
	  @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name="userid")
	    private Long userid;
	  
	  	public Users() {}

	    public Users(Long userid, String firstname, String lastname, Long teamid)
	    {
	            this.userid=userid;
	            this.firstName=firstname;
	            this.lastname=lastname;
	            this.teamid=teamid;
	    }

	    public Long getUserid() {
	        return userid;
	    }

	    public void setUserid(Long userid) {
	        this.userid = userid;
	    }

	    public String getFirstName() {
	        return firstName;
	    }

	    public void setFirstName(String firstName) {
	        this.firstName = firstName;
	    }

	    public String getLastname() {
	        return lastname;
	    }

	    public void setLastname(String lastname) {
	        this.lastname = lastname;
	    }

	    @Column(name="first_name")
	    private String firstName;

	    @Column(name="last_name")
	    private String lastname;
	    
	    @Column(name="TeamId")
	    private Long teamid;

		/**
		 * @return the teamid
		 */
		public Long getTeamid() {
			return teamid;
		}

		/**
		 * @param teamid the teamid to set
		 */
		public void setTeamid(Long teamid) {
			this.teamid = teamid;
		}

}
