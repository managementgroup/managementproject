package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name ="risks")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)

public class Risks {
	
	public Risks() {}
	
	public Risks(Long riskId, String risk, String riskStatus, Long projectId)
	
	{
		
		this.riskId=riskId;
		this.risk=risk;
		this.riskStatus=riskStatus;
		this.projectId=projectId;
		
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="riskId")
	private Long riskId;
	
	/**
	 * @return the riskId
	 */
	public Long getRiskId() {
		return riskId;
	}

	/**
	 * @param riskId the riskId to set
	 */
	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}

	/**
	 * @return the risk
	 */
	public String getRisk() {
		return risk;
	}

	/**
	 * @param risk the risk to set
	 */
	public void setRisk(String risk) {
		this.risk = risk;
	}

	/**
	 * @return the riskStatus
	 */
	public String getRiskStatus() {
		return riskStatus;
	}

	/**
	 * @param riskStatus the riskStatus to set
	 */
	public void setRiskStatus(String riskStatus) {
		this.riskStatus = riskStatus;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	@Column(name="risk")
	private String risk;
	
	@Column(name="risk_status")
	private String riskStatus;
	
	@Column(name="FK_ProjectId")
	private Long projectId;
	
	
	

}
